package Code;

public class Energiespeicher {
    private String energiespeicher_jahr;
    private int credit =50;
    private String getEnergiespeicher_health;
    private int energiespeicher_id = -1;
    private String getEnergiespeicher_name;
    private HochVoltBatterie hochVoltBatterie = new HochVoltBatterie();

    public Energiespeicher(){
        this.energiespeicher_jahr="1993";
        this.credit=50;
        this.getEnergiespeicher_health="66";
        this.energiespeicher_id=0;
        this.getEnergiespeicher_name="no name";
        hochVoltBatterie.setGewicht(9);
        hochVoltBatterie.setProduktionsJahr("9");
        hochVoltBatterie.setStateOfHealth("10");
    }

    public String getEnergiespeicher_jahr() {
        return energiespeicher_jahr;
    }

    public void setEnergiespeicher_jahr(String energiespeicher_jahr) {
        this.energiespeicher_jahr = energiespeicher_jahr;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getGetEnergiespeicher_health() {
        return getEnergiespeicher_health;
    }

    public void setGetEnergiespeicher_health(String getEnergiespeicher_health) {
        this.getEnergiespeicher_health = getEnergiespeicher_health;
    }

    public int getEnergiespeicher_id() {
        return energiespeicher_id;
    }

    public void setEnergiespeicher_id(int energiespeicher_id) {
        this.energiespeicher_id = energiespeicher_id;
    }

    public String getGetEnergiespeicher_name() {
        return getEnergiespeicher_name;
    }

    public void setGetEnergiespeicher_name(String getEnergiespeicher_name) {
        this.getEnergiespeicher_name = getEnergiespeicher_name;
    }

    public HochVoltBatterie getHochVoltBatterie() {
        return hochVoltBatterie;
    }

    public void setHochVoltBatterie(HochVoltBatterie hochVoltBatterie) {
        this.hochVoltBatterie = hochVoltBatterie;
    }

    @Override
    public String toString() {
        return "Energiespeicher{" +
                "energiespeicher_jahr='" + energiespeicher_jahr + '\'' +
                ", credit=" + credit +
                ", getEnergiespeicher_health='" + getEnergiespeicher_health + '\'' +
                ", energiespeicher_id=" + energiespeicher_id +
                ", getEnergiespeicher_name='" + getEnergiespeicher_name + '\'' +
                ", hochVoltBatterie=" + hochVoltBatterie +
                '}';
    }
}
