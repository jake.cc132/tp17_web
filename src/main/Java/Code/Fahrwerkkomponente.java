package Code;

public class Fahrwerkkomponente {

    private int credit=-1;        //## attribute credit

    private String name;        //## attribute name

    private int fahrwerkomponente_id;




    public Fahrwerkkomponente() {
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFahrwerkomponente_id() {
        return fahrwerkomponente_id;
    }

    public void setFahrwerkomponente_id(int fahrwerkomponente_id) {
        this.fahrwerkomponente_id = fahrwerkomponente_id;
    }

    @Override
    public String toString() {
        return "Fahrwerkkomponente{" +
                "credit=" + credit +
                ", name='" + name + '\'' +
                ", fahrwerkomponente_id=" + fahrwerkomponente_id +
                '}';
    }
}