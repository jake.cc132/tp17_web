package de.ostfalia.tp;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;



@WebServlet("/servertTest")
public class ServletTest extends javax.servlet.http.HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String id = request.getParameter("vehicle_id");
        System.out.println(id);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }


}
