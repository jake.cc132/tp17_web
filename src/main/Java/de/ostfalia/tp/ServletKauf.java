package de.ostfalia.tp;

import Client.*;
import Code.User;
import Code.Vehicle;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/servletKauf")
public class ServletKauf extends HttpServlet {
    User user=null;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String info_kauf = request.getParameter("info_kauf").toString();
        int credit = 0;
        int id = -1;
        Vehicle v = (Vehicle) request.getSession().getAttribute("vehicle");
        switch (info_kauf){
            case "Motor" : credit = v.getElektromotor().getCredit();id = v.getElektromotor().getElektromotroe_id();break;
            case "HochVolitbatterie": credit = v.getEnergiespeicher().getHochVoltBatterie().getCredit();id = v.getEnergiespeicher().getHochVoltBatterie().getHochvoltBatterie_id();break;
            case "Fahrzeugkomponent": credit = v.getFahrwerkkomponente().getCredit();id = v.getFahrwerkkomponente().getFahrwerkomponente_id();break;
            case "Energiespeicher": credit = v.getEnergiespeicher().getCredit();id = v.getEnergiespeicher().getEnergiespeicher_id();
            default:;
        }

        Cookie[] cookies = request.getCookies();
        System.out.println(cookies);
        String username = "username";
        for (Cookie cookie : cookies
        ) {
            if (cookie.getName().equals("username")) {
                username = cookie.getValue();
                System.out.println("get Cookie");
                System.out.println(cookie.getValue());
            }
        }

        List<User> ulist = (List<User>) request.getSession().getAttribute("Userlist");
        int user_index = -1;
        for (User u : ulist
        ) {
            user_index++;
            if (u.getUsername().equals(username)) {
                this.user = u;
                System.out.println("found user");
                System.out.println(u.getUsername());
            }
        }
        int[] aa = user.getVehicles();
        for (int i=0;i<aa.length;i++){
            if (aa[i]==id) break;
            if (aa[i]==-1){
                DoVerkauf doVerkauf=null;
                try {
                    doVerkauf = new DoVerkauf(v.getOnwer_id(),credit);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    new Client(doVerkauf).run();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                credit = user.getCredit()-credit;
                this.user.setCredit(credit);
                aa[i]=id;
                break;
            }
        }

        ulist.set(user_index,this.user);
        request.getSession().setAttribute("userlist",ulist);



        DoKaufen doKaufen = null;
        try {
            doKaufen = new DoKaufen(aa,user.getCredit(),user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            new Client(doKaufen).run();
        } catch (Exception e) {
            e.printStackTrace();
        }



        request.getRequestDispatcher("/userschnitte.jsp").forward(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
