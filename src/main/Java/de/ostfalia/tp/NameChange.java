package de.ostfalia.tp;

import Client.Client;
import Client.WriteName;
import Code.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/nameChange")
public class NameChange extends HttpServlet {
    User user;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        System.out.println(cookies);
        String username = "username";
        for (Cookie cookie : cookies
        ) {
            if (cookie.getName().equals("username")) {
                username = cookie.getValue();
                System.out.println("get Cookie");
                System.out.println(cookie.getValue());
            }
        }

        List<User> ulist = (List<User>) request.getSession().getAttribute("Userlist");
        int user_index = -1;
        for (User u : ulist
        ) {
            user_index++;
            if (u.getUsername().equals(username)) {
                this.user = u;
                System.out.println("found user");
                System.out.println(u.getUsername());
            }
        }
        this.user.setName(request.getParameter("neu_name"));
        ulist.set(user_index,this.user);
        request.getSession().setAttribute("userlist",ulist);
        user.getUsername();
        WriteName writeName = null;
        try {
            writeName = new WriteName(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            new Client(writeName).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        this.user
        request.setAttribute("user_info","name changed");
        request.getRequestDispatcher("/name.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
