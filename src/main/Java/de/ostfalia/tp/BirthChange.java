package de.ostfalia.tp;

import Client.Client;
import Client.WriteBirth;
import Code.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@WebServlet("/birthChange")
public class BirthChange extends HttpServlet {
    User user;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        System.out.println(cookies);
        String username = "username";
        for (Cookie cookie : cookies
        ) {
            if (cookie.getName().equals("username")) {
                username = cookie.getValue();
                System.out.println("get Cookie");
                System.out.println(cookie.getValue());
            }
        }

        List<User> ulist = (List<User>) request.getSession().getAttribute("Userlist");
        int user_index = -1;
        for (User u : ulist
        ) {
            user_index++;
            if (u.getUsername().equals(username)) {
                this.user = u;
                System.out.println("found user");
                System.out.println(u.getUsername());
            }
        }

        try {
            this.user.setGeburtstag(request.getParameter("neu_birth"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ulist.set(user_index,this.user);
        request.getSession().setAttribute("userlist",ulist);
        user.getUsername();
        WriteBirth writeBirth = null;
        try {
            writeBirth = new WriteBirth(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            new Client(writeBirth).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        this.user
        request.setAttribute("user_info","birthday changed");
        request.getRequestDispatcher("/birthday.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
