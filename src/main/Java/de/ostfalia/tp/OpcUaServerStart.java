//package de.ostfalia.tp;
//
//import Server.Server;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.concurrent.ExecutionException;
//
//@WebServlet(value = "/OpcUaServerStart", loadOnStartup = 1)
//public class OpcUaServerStart extends HttpServlet {
//    Server server = new Server();
//    @Override
//    public void init() throws ServletException {
//
//        try {
//            this.server.serverStart();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("opcua server start");
//        super.init();
//    }
//
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//    }
//}
