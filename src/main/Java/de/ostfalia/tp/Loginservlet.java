package de.ostfalia.tp;

import Client.FindOwner;
import Client.FindRecycler;
import Code.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

@WebServlet("/Loginservlet")
public class Loginservlet extends HttpServlet {
    String password = "";
    User user;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username").toString();
        String password2 = req.getParameter("password").toString();

        int times = 0;
        if (req.getSession().getAttribute("pass_" + username) != null) {
            times = (int) req.getSession().getAttribute("pass_" + username);
        }

        if (times < 3) {
            FindOwner find = null;
            try {
                find = new FindOwner();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                this.user = find.getFind(username);
                System.out.println(user.toString());
                password = user.getPassword();

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (this.user.getUsername() == null) {
                System.out.println("will try to find rec");
                FindRecycler findRecycler = null;
                try {
                    findRecycler = new FindRecycler();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    this.user = findRecycler.getFind(username);
                    System.out.println(user.toString());
                    password = user.getPassword();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(this.user.getUsername());
            }


            if (password.equals(password2)) {
                req.setAttribute("User", user);
                req.getSession().setAttribute("pass_"+username,0);
                req.getSession().setMaxInactiveInterval(24*60*60);
                req.getRequestDispatcher("/userschnitte.jsp").forward(req, resp);
                System.out.println(user.toString());

            }
//            else if (password.equals("#####")) {
//                req.setAttribute("Flag", "Missed");
//                req.getRequestDispatcher("/login.jsp").forward(req, resp);
//                System.out.println("NO");
//                System.out.println("Passwordin " + password2);
//                System.out.println("Password " + password);
//
//            }
            else {
                req.setAttribute("Flag", "Failed");
                req.getRequestDispatcher("/login.jsp").forward(req, resp);
                req.getSession().setAttribute("pass_"+username,times+1);
                System.out.println("NO");
                System.out.println("Passwordin " + password2);
                System.out.println("Password " + password);
            }

        }else {
            req.setAttribute("3times","3 Mals Falsches Password, Bitte nach 24 Stunden nochmal Versuchen!");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
