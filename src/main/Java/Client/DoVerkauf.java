package Client;

import Code.User;
import Code.Vehicle;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class DoVerkauf implements ClientData {
    private User user;
    private int credit;
    private int id;
    BrowseUser browseUser = new BrowseUser("Credit");
    BrowseUser browserUser2 = new BrowseUser("User_ID");
    Map<String, NodeId> map = browseUser.getName_pass_map();
    Map<String, NodeId> map3 = browserUser2.getName_pass_map();

    Map<String, NodeId> map_v = new BrowseVehicle().getMap();
    FindVehicle findVehicle = new FindVehicle();


    private final Logger logger = LoggerFactory.getLogger(getClass());
    public DoVerkauf(int id, int credit) throws Exception {
        this.id = id;
        this.credit = credit;
    }

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        findVehicle.setMap(map_v);
        Vehicle v = findVehicle.getFind(map_v);
        int credit_o=0;


        if (v.getOnwer_id()==id) {
            int user_id = v.getOnwer_id();
            String username = "";
            for (Map.Entry<String, NodeId> entry : map3.entrySet()) {
                if ((int)client.getAddressSpace().createVariableNode(entry.getValue()).getValue().get() == user_id) {
                    username = entry.getKey();
                }
            }
            System.out.println("user id get right");
            NodeId credit_node = map.get(username);


            credit_o = (int) client.getAddressSpace().createVariableNode(credit_node).getValue().get()+credit;
            DataValue dv = new DataValue(new Variant(credit_o));


            CompletableFuture<StatusCode> f =
                    client.writeValue(credit_node, dv);
            StatusCode statusCodes = f.get();
            if (statusCodes.isGood()) {
                logger.info("Wrote '{}' to nodeId={} statusCode = {}", v, credit_node, statusCodes);
            }
            System.out.println("should beeeeeeeeewrote");
            future.complete(client);
        }

    }
}
