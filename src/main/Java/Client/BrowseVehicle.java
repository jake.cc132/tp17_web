package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.nodes.Node;
import org.eclipse.milo.opcua.sdk.client.nodes.UaNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaObjectNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class BrowseVehicle implements ClientData {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Map<String, NodeId> map = new HashMap<>();
    String name = "";
    String nodeid = "";
    NodeId password;

    public BrowseVehicle() {
//        this.nodeid = nodeid;
    }

    public Map<String, NodeId> getMap() throws Exception {
        BrowseVehicle browseVehicle = new BrowseVehicle();
        new Client(browseVehicle).run();
        return browseVehicle.map;
    }

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        NodeId rootnode = new NodeId(2, "Vehicles");
        Node vehicle1 = client.getAddressSpace().browse(rootnode).get().get(0); // ein vehicle
        List<Node> vehicleNodes = client.getAddressSpace().browse(vehicle1.getNodeId().get()).get(); //object node elektromotor fahrkompon..
        for (Node node :
                vehicleNodes) {
            List<Node> partNode = client.getAddressSpace().browse(node.getNodeId().get()).get(); //variable node
            if (node.getBrowseName().get().getName().equals("Vehicle_Credit")){
                map.put("Vehicle_Credit",node.getNodeId().get());
            }
            if (node.getBrowseName().get().getName().equals("Vehicle_id")){
                map.put("Vehicle_id",node.getNodeId().get());
            }
            if (node.getBrowseName().get().getName().equals("Owner_id_Vehicle")){
                map.put("Owner_id_Vehicle",node.getNodeId().get());
            }

            for (Node node_var :
                    partNode) {
                if (node_var.getBrowseName().get().getName().equals("HochVoltBatterie")) {
                    List<Node> batts =  client.getAddressSpace().browse(node_var.getNodeId().get()).get();
                    for (Node node_batt:batts
                    ) {
                        map.put(node_batt.getBrowseName().get().getName(),node_batt.getNodeId().get());
                    }
                } else {
                    map.put(node_var.getBrowseName().get().getName(), node_var.getNodeId().get());
                }
            }


        }

    }
}

