package Client;

import Code.User;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.nodes.UaObjectNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.crypto.Data;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.concurrent.CompletableFuture;

public class FindOwner implements ClientData {
    private String ident = "1";
    private User user = new User();
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public FindOwner() throws ParseException {
    }

    public User getFind(String ident) throws Exception {

        this.ident = ident;
        FindOwner finduser = new FindOwner();
        //set username
        finduser.setIdent(ident);
        new Client(finduser).run();

        return finduser.user;
    }

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        // start browsing at root folder
        NodeId nodeID = new NodeId(2, "Owner/Owner");
        findNode(this.ident, client, nodeID, this.user);
        future.complete(client);
    }

    private void findNode(String ident, OpcUaClient client, NodeId nodeId, User user) throws Exception {
        UaObjectNode uaObjectNode = (UaObjectNode) client.getAddressSpace().createObjectNode(nodeId);
        UaVariableNode uaVariableNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "User_Name")).get();
        DataValue value = uaVariableNode.readValue().get();
        if (ident.equals(value.getValue().getValue().toString())) {
            logger.info("find success");
            System.out.println("find success");
            UaVariableNode PasswordNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "Password")).get();
            UaVariableNode BirthdayNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "Birthday")).get();
            UaVariableNode CreditNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "Credit")).get();
            UaVariableNode EmailNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "Email")).get();
            UaVariableNode UserIdNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "User_ID")).get();
            UaVariableNode UsertypeNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "Usertype")).get();
            UaVariableNode NameNode = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2, "Name")).get();
            UaVariableNode Angebot = (UaVariableNode) uaObjectNode.getVariableComponent(new QualifiedName(2,"Angebot")).get();
            DataValue Passwordvalue = PasswordNode.readValue().get();
            System.out.println("password");
            DataValue Birthdayvalue = BirthdayNode.readValue().get();
            DataValue Creditvalue = CreditNode.readValue().get();
            DataValue Emailvalue = EmailNode.readValue().get();
            DataValue UserIdvalue = UserIdNode.readValue().get();
            DataValue Usertypevalue = UsertypeNode.readValue().get();
            DataValue Namevalue = NameNode.readValue().get();
            DataValue AngebotValue = Angebot.readValue().get();
            System.out.println(Passwordvalue.getValue().getValue());
            user.setEmail(Emailvalue.getValue().getValue().toString());
            user.setGeburtstag((String) Birthdayvalue.getValue().getValue());
            user.setId(Integer.parseInt(UserIdvalue.getValue().getValue().toString()));
            user.setName(Namevalue.getValue().getValue().toString());
            user.setUsername(value.getValue().getValue().toString());
            user.setPassword(Passwordvalue.getValue().getValue().toString());
            user.setUsertype(Usertypevalue.getValue().getValue().toString());
            user.setCredit(Integer.parseInt(Creditvalue.getValue().getValue().toString()));
            int[] i_arr =  new int[10];
            for (int i =0;i<10;i++){
                i_arr[i] = (int) Array.get(AngebotValue.getValue().getValue(),i);
            }
            user.setVehicles(i_arr);


            logger.info("Password: " + Passwordvalue.getValue().getValue());
        } else {
            System.out.println("can not find");

        }

    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public User getUser() {
        return user;
    }

//    public void setUser(User user) {
//        this.user = user;
//    }
}
