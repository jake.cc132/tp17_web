package Client;

import Code.HochVoltBatterie;
import Code.Vehicle;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.nodes.UaObjectNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class FindVehicle implements ClientData {
    Map<String,NodeId> map;
    Vehicle vehicle = new Vehicle();
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public FindVehicle(){}
    public void setMap(Map<String,NodeId> map){
        this.map=map;
    }
    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        System.out.println("start");

        this.vehicle.getEnergiespeicher().setEnergiespeicher_id((int)client.getAddressSpace().createVariableNode(map.get("Energiespeicher_id")).getValue().get());
        this.vehicle.getEnergiespeicher().setGetEnergiespeicher_name(client.getAddressSpace().createVariableNode(map.get("Energiespericher_name")).getValue().get().toString());
        this.vehicle.getEnergiespeicher().setCredit((int)client.getAddressSpace().createVariableNode(map.get("Energiespeicher_credit")).getValue().get());
        this.vehicle.getEnergiespeicher().setEnergiespeicher_jahr(client.getAddressSpace().createVariableNode(map.get("Energiespeicer_jahr")).getValue().get().toString());
        this.vehicle.getEnergiespeicher().setGetEnergiespeicher_health(client.getAddressSpace().createVariableNode(map.get("Energiespeicher_health")).getValue().get().toString());
        HochVoltBatterie hv = new HochVoltBatterie();
        hv.setStateOfHealth(client.getAddressSpace().createVariableNode(map.get("StateOfHealth")).getValue().get().toString());
        hv.setProduktionsJahr(client.getAddressSpace().createVariableNode(map.get("ProduktionsJahr")).getValue().get().toString());
        hv.setGewicht((int)client.getAddressSpace().createVariableNode(map.get("Gewicht")).getValue().get());
        hv.setCredit((int)client.getAddressSpace().createVariableNode(map.get("HochVoltBatterie_credit")).getValue().get());
        hv.setHochvoltBatterie_id((int)client.getAddressSpace().createVariableNode(map.get("HochVoltBatterie_id")).getValue().get());
        this.vehicle.getEnergiespeicher().setHochVoltBatterie(hv);

        this.vehicle.getFahrwerkkomponente().setCredit((int)client.getAddressSpace().createVariableNode(map.get("Fahrzeugkomponent_credit")).getValue().get());
        this.vehicle.getFahrwerkkomponente().setFahrwerkomponente_id((int)client.getAddressSpace().createVariableNode(map.get("Fahrzeugkomponent_id")).getValue().get());
        this.vehicle.getFahrwerkkomponente().setName(client.getAddressSpace().createVariableNode(map.get("Fahrzeugkomponent_name")).getValue().get().toString());

        this.vehicle.setVehicle_id((int)client.getAddressSpace().createVariableNode(map.get("Vehicle_id")).getValue().get());
        this.vehicle.setCredit((int)client.getAddressSpace().createVariableNode(map.get("Vehicle_Credit")).getValue().get());

        this.vehicle.getElektromotor().setCredit((int)client.getAddressSpace().createVariableNode(map.get("Elektromotor_credit")).getValue().get());
        this.vehicle.getElektromotor().setName(client.getAddressSpace().createVariableNode(map.get("Elektromotor_name")).getValue().get().toString());
        this.vehicle.getElektromotor().setElektromotroe_id((int)client.getAddressSpace().createVariableNode(map.get("Elektromotor_id")).getValue().get());

        this.vehicle.setOnwer_id((int)client.getAddressSpace().createVariableNode(map.get("Owner_id_Vehicle")).getValue().get());
        future.complete(client);
    }

    public Vehicle getFind(Map<String,NodeId> map) throws Exception {
        this.map = map;
        FindVehicle findVehicle = new FindVehicle();
        findVehicle.setMap(map);
        new Client(findVehicle).run();
        return findVehicle.vehicle;
    }




}
