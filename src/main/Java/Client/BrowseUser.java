package Client;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.nodes.Node;
import org.eclipse.milo.opcua.sdk.client.api.nodes.VariableNode;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.awt.datatransfer.ClipboardTransferable;
import sun.misc.Cleaner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class BrowseUser implements ClientData{

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private  Map<String,NodeId> name_pass_map = new HashMap<>();
    String name = "";
    String nodeid ="";
    NodeId password;

    public BrowseUser(String node){
        this.nodeid=node;
    }
    public Map<String,NodeId> getName_pass_map() throws Exception{
        BrowseUser browseUser = new BrowseUser(nodeid);
        new Client(browseUser).run();
        return browseUser.name_pass_map;
    }

    @Override
    public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception {
        client.connect().get();
        NodeId rootnode = new NodeId(2,"Users");
        List<Node> userNodes= client.getAddressSpace().browse(rootnode).get();
        List<Node> ownerNodes = client.getAddressSpace().browse(userNodes.get(0).getNodeId().get()).get();
        List<Node> recNodes = client.getAddressSpace().browse(userNodes.get(1).getNodeId().get()).get();
        for (Node node : ownerNodes
             ) {
            List<Node> owner = client.getAddressSpace().browse(node.getNodeId().get()).get();
            for (Node owner_node: owner){
                if(owner_node.getBrowseName().get().getName().equals("User_Name")){
                    name = ((UaVariableNode)owner_node).getValue().get().toString();
                }
                if(owner_node.getBrowseName().get().getName().equals(nodeid)){
                    password = owner_node.getNodeId().get();
                }
                System.out.println("11111");
            }

            //retrun with user name and the selected nodeid e.g. password, birthday
            name_pass_map.put(name,password);
        }

        for (Node node : recNodes
        ) {
            List<Node> rec = client.getAddressSpace().browse(node.getNodeId().get()).get();
            for (Node rec_node: rec){
                if(rec_node.getBrowseName().get().getName().equals("User_Name")){
                    name = ((UaVariableNode)rec_node).getValue().get().toString();
                }
                if(rec_node.getBrowseName().get().getName().equals(nodeid)){
                    password = rec_node.getNodeId().get();
                }
            }
            name_pass_map.put(name,password);
        }



    }
}
