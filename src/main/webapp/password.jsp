<%--
  Created by IntelliJ IDEA.
  User: aaa
  Date: 2019/12/13
  Time: 17:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>

<style type="text/css">
    * {
        margin: 0;
        padding: 0;
    }

    .header {
        width: auto;
        height: 150px;
        background-color: rgb(94, 255, 115);
        font-size: 36px;
        line-height: 150px;
        color: white;
    }

    .left1 {
        width: 600px;
        height: 200px;
        background-color: rgb(115, 115, 115);
        font-size: 24px;
        margin-bottom: 10px;
        display: block;
        text-align: center;
        line-height: 200px;
        text-decoration: none;
        color: black;
        float: left;

    }

    .right {
        width: auto;
        height: auto;
        background-color: wheat;
        float: left;

    }
</style>
<body style="background-color: wheat;">
<div class="header"> Informationsmarktplatz</div>
<div class="content">
    <!--Inhalt des link Seite-->
    <div class="left">
        <ul class="left1">
            <li id="list1">Meine Konto</li>
        </ul>

        <div class="right">
            <form id="meld " action="/tp_web_war/passwordVerify" method="post">
                <table width="200px" height="150" border="">
                    <tr>
                        <td>Altes Password</td>
                        <td>
                            <input type="text" name="alt_password">
                        </td>
                    </tr>
                    <tr>
                        <td>Neues Password</td>
                        <td>
                            <input type="password" name="neu_password">
                        </td>
                    <tr>
                        <td>
                            <input type="submit" value="Password Verändern" id="pass_change">
                        </td>
                        <td>
                            <%
                            String pass_in="";
                            if (request.getAttribute("password_info")!=null){
                                pass_in = request.getAttribute("password_info").toString();
                            }
                            if (pass_in.equals("success changed")){
                                out.println("success changed");
                            }else if(pass_in.equals("failed changed"))
                                out.println("alt password falsch");
                            %>
                        </td>
                    </tr>
                </table>
            </form>

            <form action="/tp_web_war/userschnitte.jsp">
                <input type="submit" value="zurück">
            </form>


        </div>
</body>
</html>
