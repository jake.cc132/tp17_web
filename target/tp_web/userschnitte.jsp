<%@ page import="Code.User" %>
<%@ page import="java.net.HttpCookie" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.text.CollationKey" %>
<%@ page import="io.netty.handler.codec.http.HttpRequest" %>
<%@ page import="Code.Vehicle" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.eclipse.milo.opcua.stack.core.types.builtin.NodeId" %>
<%@ page import="Client.FindVehicle" %>
<%@ page import="Client.BrowseVehicle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>informationmarktplatz</title>
    <!--CSS Style einrichten -->
    <style type="text/css">
        /*Polster und Ränder von Body entfernen*/

        * {
            margin: 0;
            padding: 0;
        }

        /*CSS von link Seite einrichten*/
        .left1 {
            list-style: none;
            float: left;
            margin-right: 10px;
        }

        .left1 li {
            width: 600px;
            height: 200px;
            background-color: rgb(115, 115, 115);
            font-size: 24px;
            margin-bottom: 10px;

        }

        .left1 a {
            display: block;
            width: 100%;
            text-align: center;
            line-height: 200px;
            text-decoration: none;


        }

        .nav a:hover {

            background-color: black;
        }

        /*CSS von Kopf einrichten*/
        .header {
            width: auto;
            height: 150px;
            background-color: rgb(94, 255, 115);
            font-size: 36px;
            line-height: 150px;
            color: white;
        }

        /*CSS von Kopf einrichten*/
        .right {
            width: auto;
            height: auto;
            background-color: wheat;
            float: left;

        }

        /*CSS Von Search button einrichten*/
        .search button {
            flex: 1;
            width: auto;
            height: 30px;
            background-color: black;
            color: white;
            border-style: none;
            outline: none;
        }

        .search button:hover {
            font-size: 16px;
            background-color: green;
        }

        /*CSS Von Abfrageergebnis einrichten*/

        .Result {

            width: 300px;
            height: 200px;
            margin: auto;
            overflow: auto;
            margin-top: 30px;
            background-color: antiquewhite;
        }

        .close {
            margin-right: 5px;
            float: right;
        }

        .box5 {
            width: 800px;
            height: 300px;
            background-color: ghostwhite;
            margin-bottom: 10px;
        }

        .box6 {
            width: 30%;
            height: 80%;
            background-image: url(resources/Elektormotor.jpg);
            /*background-color: ;*/
            float: left;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center left;
        }

        .box7 {
            width: 68%;
            height: 80%;
            float: left;
        }

        .box8 {
            width: 30%;
            height: 80%;
            background-image: url(resources/Energiespeicher.jpg);
            /*background-color: ;*/
            float: left;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center left;
        }

        .box9 {
            width: 30%;
            height: 80%;
            background-image: url(resources/Fahrzeugkomponer.png);
            /*background-color: ;*/
            float: left;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center left;
        }

        .kopf {
            margin-bottom: 30px;
            text-align: center;
            font-size: 24px;

        }

    </style>

    <!--JS für link Seite einrichten, um die Funktion des Wahlens zu erreichen-->
    <script type="text/javascript">


        function funcShow(id) {
            for (var i = 0; i < 5; i++) {
                var divInfo = document.getElementById('div' + (i + 1));
                divInfo.style.display = 'none';
            }
            var div = document.getElementById('div' + id);
            div.style.display = 'block';
        }


    </script>


</head>

<!--Hintergrundfarbe einrichten-->
<body style="background-color: wheat;">

<!--Inhalt des Kopfs -->

<div class="header"> Informationsmarktplatz</div>

<!--Inhalt der Mitte-->

<div class="content">
    <!--Inhalt des link Seite-->
    <div id="left">
        <ul class="left1">

            <li id="list1"><a href="javascript:funcShow(1)">Startseit</a></li>
            <li id="list2"><a href="javascript:funcShow(2)">Mein Konto</a></li>
            <li id="list3"><a href="javascript:funcShow(3)">Meine vorhandenen Information</a></li>
            <li id="list4"><a href="javascript:funcShow(4)">Meine angebotenen Information</a></li>
            <li id="list5"><a href="javascript:funcShow(5)">Über den Marktplatz</a></li>
        </ul>

        <!--Inhalt des rechts Seite-->
        <div class="right">
            <div id="div1" style="display:block">
                <div class="search">
                    <form id="form_id" action="/tp_web_war/servletFindNode" method="post">
                        <input type="text" placeholder="id enter" name="id">
                        <button type="submit" id="showBtn">Suchen</button>
                    </form>
                    <td><%
                        String exists = "none";
                        Vehicle vehicle = new Vehicle();
                        if (request.getAttribute("kost") == null) {
                        } else if (request.getAttribute("kost").toString().equals("-1")) {
                            out.println("<h3>Vehicle</h3>");
                            out.println("vehicle nicht exists");
                            System.out.println(request.getAttribute("kost").toString());
                        } else {
                            exists = "block";
                            System.out.println(request.getAttribute("kost").toString());
                            vehicle = (Vehicle) request.getAttribute("vehicle");
                            request.getSession().setAttribute("vehicle",vehicle);

                        }

//                        System.out.println(vehicle.toString());
                    %></td>
                </div>
                <div class="box5" id="motor" style="display: <%=exists%>;">
                    <div class="box6">
                    </div>
                    <form action="/tp_web_war/servletKauf" method="post">
                        <div class="box7">
                            <div class="kopf">
                                Motor
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td colspan="1">Zur verfügung stehende Information</td>
                                </tr>
                                <tr>
                                    <td>Motor Name</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>Demontageanleitung</td>
                                    <td>×</td>
                                </tr>
                                <tr>
                                    <td>Rohstoffzusammensetzung</td>
                                    <td>×</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Credit</td>
                                    <td><%=vehicle.getElektromotor().getCredit()%>
                                    </td>
                                </tr>
                            </table>

                            <div align="center">
                                <button type="submit" name = "info_kauf" value="Motor" style="margin-top: 50px;"> INFORMATION KAUFEN</button>
                            </div>

                        </div>
                    </form>
                </div>


                <div class="box5" id="energiespeicher" style="display: <%=exists%>;">
                    <div class="box8">

                    </div>
                    <form action="/tp_web_war/servletKauf" method="post">
                        <div class="box7">
                            <div class="kopf">
                                Energiespeicher
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td colspan="1">Zur verfügung stehende Information</td>

                                </tr>
                                <tr>
                                    <td>Energiespeicher_Health</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>Demontageanleitung</td>
                                    <td>×</td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher_Name</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher_Jahr</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Credit</td>
                                    <td><%=vehicle.getEnergiespeicher().getCredit()%>
                                    </td>
                                </tr>
                            </table>

                            <div align="center">
                                <button type="submit" name = "info_kauf" value="Energiespeicher" style="margin-top: 50px;"> INFORMATION KAUFEN</button>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="box5" id="kompotoner" style="display: <%=exists%>;">
                    <div class="box9">

                    </div>
                    <form action="/tp_web_war/servletKauf" method="post">
                        <div class="box7">
                            <div class="kopf">
                                Fahrzeugkomponent
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td colspan="1">Zur verfügung stehende Information</td>

                                </tr>
                                <tr>
                                    <td>Fahrzeugkomponent Name</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>Demontageanleitung</td>
                                    <td>×</td>
                                </tr>
                                <tr>
                                    <td>Rohstoffzusammensetzung</td>
                                    <td>×</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Credit</td>
                                    <td><%=vehicle.getFahrwerkkomponente().getCredit()%>
                                    </td>
                                </tr>
                            </table>

                            <div align="center">
                                <button type="submit" name = "info_kauf" value="Fahrzeugkomponent" style="margin-top: 50px;"> INFORMATION KAUFEN</button>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="box5" id="batterie" style="display: <%=exists%>;">
                    <div class="box8">

                    </div>
                    <form action="/tp_web_war/servletKauf" method="post">
                        <div class="box7">
                            <div class="kopf">
                                HochVoltBatterie
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td colspan="1">Zur verfügung stehende Information</td>

                                </tr>
                                <tr>
                                    <td>HochVoltBatterie Gewicht</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>HochVoltBatterie Name</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>HochVolitbatterie ProduktionsJahr</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td>HochVolitbatterie Jahr</td>
                                    <td>√</td>
                                </tr>
                                <tr>
                                    <td colspan="1">Credit</td>
                                    <td><%=vehicle.getEnergiespeicher().getHochVoltBatterie().getCredit()%>
                                    </td>
                                </tr>
                            </table>

                            <div align="center">
                                <button type="submit" name = "info_kauf" value="HochVolitbatterie" style="margin-top: 50px;"> INFORMATION KAUFEN</button>
                            </div>

                        </div>
                    </form>
                </div>

            </div>

            <!--Table von Anmelden und rigistieren-->
            <div id="div2" style="display:none">
                <div style="float: left;">
                    <table border="">
                        <%
                            User user = (User) request.getAttribute("User");
                            Cookie[] cookies = request.getCookies();
                            if (user == null) {
                                List<User> ulist = (List<User>) request.getSession().getAttribute("Userlist");
//                                Cookie[] cookies = request.getCookies();
                                String username = "username";
                                for (Cookie cookie : cookies
                                ) {
                                    if (cookie.getName().equals("username")) {
                                        username = cookie.getValue();
                                    }
                                }
                                for (User u : ulist) {
                                    if (u.getUsername().equals(username))
                                        user = u;
                                }
                            }
                            int[] aa = user.getVehicles();
                            System.out.println("vehicle_id_ = " + aa[0]);
                            Cookie userCookie = new Cookie("username", user.getUsername());
//                            userCookie.setDomain();
                            response.addCookie(userCookie);
                            List<User> userList = new ArrayList<>();
                            if (!userList.contains(user)) {
                                userList.add(user);
                            }
                            session.setAttribute("Userlist", userList);%>
                        <tr>
                            <td>User_Id</td>
                            <td><%=user.getId()%>
                            </td>
                        </tr>

                        <tr>
                            <td>Username</td>
                            <td><%=user.getUsername()%>
                            </td>
                        </tr>

                        <tr>
                            <td>Name</td>

                            <td><%=user.getName()%>
                            </td>
                            <td>
                                <form action="/tp_web_war/name.jsp" method="post">
                                    <button type="submit">Bearbeiten
                                    </button>
                                </form>

                            </td>
                        </tr>

                        <tr>
                            <td>Birthday</td>
                            <td><%=user.getGeburtstag()%>
                            </td>
                            <td>
                                <form action="/tp_web_war/birthday.jsp" method="get">
                                    <input type="submit" value="Bearbeiten">
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <td>E-Mail</td>
                            <td><%=user.getEmail()%>
                            </td>
                            <td>
                                <form action="/tp_web_war/email.jsp" method="get">
                                    <input type="submit" value="Bearbeiten">
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <td>Credit</td>
                            <td><%=user.getCredit()%>
                            </td>
                            <td>
                                <form action="/tp_web_war/password.jsp" method="get">
                                    <input type="submit" value="Password Ändern">
                                </form>
                            </td>
                        </tr>

                    </table>


                </div>

            </div>
            <!--  Andere Information -->
            <%
                String user_hasVehicle = "none";
                Map<String, NodeId> map = new BrowseVehicle().getMap();
                FindVehicle f = new FindVehicle();
                f.setMap(map);
                Vehicle vehicles = f.getFind(map);
                for (int i : user.getVehicles()) {
                    if (i == vehicles.getVehicle_id()) {
                        user_hasVehicle = "block";
                        break;
                    }
                }
            %>
            <div id="div4" style="display:none">

                <div class="box5" style="display: <%=user_hasVehicle%>;">
                    <div class="box6">
                    </div>
                    <form action="" method="post">
                        <div class="box7">
                            <div class="kopf">
                                Elektromotor
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>Motor_id</td>
                                    <td><%=vehicles.getElektromotor().getElektromotroe_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Motor_Name</td>
                                    <td><%=vehicles.getElektromotor().getName()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Motor_Credit</td>
                                    <td><%=vehicles.getElektromotor().getCredit()%>
                                    </td>
                                </tr>

                            </table>


                        </div>
                    </form>
                </div>


                <div class="box5" style="display: <%=user_hasVehicle%>;">
                    <div class="box8">

                    </div>
                    <form>
                        <div class="box7">
                            <div class="kopf">
                                Energiespeicher
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>Energiespeicher id</td>
                                    <td><%=vehicles.getEnergiespeicher().getEnergiespeicher_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher_Health</td>
                                    <td><%=vehicles.getEnergiespeicher().getGetEnergiespeicher_health()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher Name</td>
                                    <td><%=vehicles.getEnergiespeicher().getGetEnergiespeicher_name()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher Jahr</td>
                                    <td><%=vehicles.getEnergiespeicher().getEnergiespeicher_jahr()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Credit</td>
                                    <td><%=vehicles.getEnergiespeicher().getCredit()%>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </form>
                </div>

                <div class="box5" style="display: <%=user_hasVehicle%>;">
                    <div class="box9">

                    </div>
                    <form>
                        <div class="box7">
                            <div class="kopf">
                                Fahrzeugkomponent
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>Fahrzeugkomponent id</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getFahrwerkomponente_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fahrzeugkomponent Name</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getName()%>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Fahrzeugkomponent Credit</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getFahrwerkomponente_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Credit</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getCredit()%>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </form>
                </div>

                <div class="box5" style="display: <%=user_hasVehicle%>;">
                    <div class="box8">

                    </div>
                    <form>
                        <div class="box7">
                            <div class="kopf">
                                HochVoltBatterie
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>HochVoltBatterie Gewicht</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getGewicht()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>HochVoltBatterie Jahr</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getProduktionsJahr()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>StateOfHealth</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getStateOfHealth()%>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Credit</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getCredit()%>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </form>
                </div>


            </div>


            <div id="div3" style="display:none">
                <%
                    String[] rec_has = {"none","none","none","none"};
                    for (int i : user.getVehicles()) {
                        if (i == vehicles.getElektromotor().getElektromotroe_id()) {
                            rec_has[0]="block;";
                        }else if (i == vehicles.getEnergiespeicher().getEnergiespeicher_id()){
                            rec_has[1]="block";
                        }else if (i == vehicles.getFahrwerkkomponente().getFahrwerkomponente_id()){
                            rec_has[2]="block";
                        }else if (i == vehicles.getEnergiespeicher().getHochVoltBatterie().getHochvoltBatterie_id()){
                            rec_has[3]="block";
                        }
                        System.out.println(i);
                    }




                %>

                <div class="box5" style="display: <%=rec_has[0]%>;">
                    <div class="box6">
                    </div>
                    <form action="" method="post">
                        <div class="box7">
                            <div class="kopf">
                                Elektromotor
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>Motor_id</td>
                                    <td><%=vehicles.getElektromotor().getElektromotroe_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Motor_Name</td>
                                    <td><%=vehicles.getElektromotor().getName()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Motor_Credit</td>
                                    <td><%=vehicles.getElektromotor().getCredit()%>
                                    </td>
                                </tr>

                            </table>


                        </div>
                    </form>
                </div>


                <div class="box5" style="display: <%=rec_has[1]%>;">
                    <div class="box8">

                    </div>
                    <form>
                        <div class="box7">
                            <div class="kopf">
                                Energiespeicher
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>Energiespeicher id</td>
                                    <td><%=vehicles.getEnergiespeicher().getEnergiespeicher_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher_Health</td>
                                    <td><%=vehicles.getEnergiespeicher().getGetEnergiespeicher_health()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher Name</td>
                                    <td><%=vehicles.getEnergiespeicher().getGetEnergiespeicher_name()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Energiespeicher Jahr</td>
                                    <td><%=vehicles.getEnergiespeicher().getEnergiespeicher_jahr()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Credit</td>
                                    <td><%=vehicles.getEnergiespeicher().getCredit()%>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </form>
                </div>

                <div class="box5" style="display: <%=rec_has[2]%>;">
                    <div class="box9">

                    </div>
                    <form>
                        <div class="box7">
                            <div class="kopf">
                                Fahrzeugkomponent
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>Fahrzeugkomponent id</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getFahrwerkomponente_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fahrzeugkomponent Name</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getName()%>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Fahrzeugkomponent Credit</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getFahrwerkomponente_id()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Credit</td>
                                    <td><%=vehicles.getFahrwerkkomponente().getCredit()%>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </form>
                </div>

                <div class="box5" style="display: <%=rec_has[3]%>;">
                    <div class="box8">

                    </div>
                    <form>
                        <div class="box7">
                            <div class="kopf">
                                HochVoltBatterie
                            </div>
                            <table border="0" align="center">
                                <tr>
                                    <td>HochVoltBatterie Gewicht</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getGewicht()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>HochVoltBatterie Jahr</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getProduktionsJahr()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>StateOfHealth</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getStateOfHealth()%>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Credit</td>
                                    <td><%=vehicles.getEnergiespeicher().getHochVoltBatterie().getCredit()%>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </form>
                </div>
            </div>
            <div id="div5" style="display:none">Über den Marktplatz</div>
        </div>

    </div>
</div>


</body>
</html>
